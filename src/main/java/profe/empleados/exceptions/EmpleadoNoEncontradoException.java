package profe.empleados.exceptions;

public class EmpleadoNoEncontradoException extends EmpleadosException {

	public EmpleadoNoEncontradoException() {
		// TODO Auto-generated constructor stub
	}

	public EmpleadoNoEncontradoException(String message) {
		super(message);
		// TODO Auto-generated constructor stub
	}

	public EmpleadoNoEncontradoException(Throwable cause) {
		super(cause);
		// TODO Auto-generated constructor stub
	}

	public EmpleadoNoEncontradoException(String message, Throwable cause) {
		super(message, cause);
		// TODO Auto-generated constructor stub
	}

	public EmpleadoNoEncontradoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
		// TODO Auto-generated constructor stub
	}

}
