package profe.empleados.empleadosweb.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import profe.empleados.empleadosweb.services.DepartamentosService;
import profe.empleados.empleadosweb.services.EmpleadosService;
import profe.empleados.model.Empleado;

@Controller
@RequestMapping("/listaDepartamentos")
public class DepartamentosWebController {

	@Autowired
	private DepartamentosService dptoService;
	
	@GetMapping
	public String muestraDepartamentos(Model model) {
		model.addAttribute("listaDepartamentos", dptoService.getAllDepartamentos());
		return "mostrar-departamentos";
	}

}
